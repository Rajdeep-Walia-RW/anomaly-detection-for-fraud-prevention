# Anomaly Detection For Fraud Prevention 

Fraud detection is a major topic in the current society, particularly for insurance and 
financial institutions. As many other problems, it can be tackled using machine learning 
and AI techniques. However, due to the particular characteristic of fraud, where only 
one of ever thousand or million transaction are fraudulent, the used of supervised 
techniques struggle in these scenarios. This is due to both the lack of large and balance 
training sets and the overwhelming amount of false positives highlighted by the system 
when uploaded to production. 
 
In this project we propose to explore the use of unsupervised machine learning 
techniques able to address fraud detection. By applying this techniques we should be 
able to model normality and detect any deviation from this normality as an anomaly or 
potential fraud. We aim to evaluate this methodologies in  fraud datasets such as 
https://www.kaggle.com/mlg-ulb/creditcardfraud and https://www.kaggle.com/ntnu-testimon/banksim1

## Objectives - 

• Study current unsupervised machine learning techniques, particularly state-of-the-
art on fraud detection.  

•  Investigate how transaction features cane be extracted to differentiate normal and 
abnormal cases.  

•  Investigate unsupervised machine learning techniques based on clustering and 
autoencoder to model normal transaction. 

• Implement an anomaly detection mechanism that using the previous model can 
detect outliers as potential fraud cases.

• Evaluate the performance of the proposed system and compare it against the state 
of the art in the field using standard datasets and appropriate metrics. 
